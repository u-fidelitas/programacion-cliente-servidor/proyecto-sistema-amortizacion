package DTO;

import java.util.*;

public class SistemaAmortizacionDTO {

    public double interesAnual;
    public int periodo;
    public ArrayList<Double> cuotasAmortizacion;
    public ArrayList<Double> cuotasInteres;
    public double totalPrestamo;
    public String moneda;
    public String tipoSistema;
    public String fecha,nombre;
    public double venta,compra;

    public SistemaAmortizacionDTO(String nom, double interesAnual, int periodo, double totalPrestamo, String moneda, String tipoSistema) {
        this.interesAnual = interesAnual;
        this.periodo = periodo;
        this.totalPrestamo = totalPrestamo;
        this.moneda = moneda;
        this.tipoSistema = tipoSistema;
        cuotasAmortizacion = new ArrayList<Double>();
        cuotasInteres = new ArrayList<Double>();
        fecha = "";
        compra = 0.0d;
        venta = 0.0d;
        nombre = nom;
    }        

    public double getInteresAnual() {
        return interesAnual;
    }

    public void setInteresAnual(double interesAnual) {
        this.interesAnual = interesAnual;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public ArrayList<Double> getCuotasAmortizacion() {
        return cuotasAmortizacion;
    }

    public void setCuotasAmortizacion(ArrayList<Double> cuotasAmortizacion) {
        this.cuotasAmortizacion = cuotasAmortizacion;
    }

    public ArrayList<Double> getCuotasInteres() {
        return cuotasInteres;
    }

    public void setCuotasInteres(ArrayList<Double> cuotasInteres) {
        this.cuotasInteres = cuotasInteres;
    }

    public double getTotalPrestamo() {
        return totalPrestamo;
    }

    public void setTotalPrestamo(double totalPrestamo) {
        this.totalPrestamo = totalPrestamo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getTipoSistema() {
        return tipoSistema;
    }

    public void setTipoSistema(String tipoSistema) {
        this.tipoSistema = tipoSistema;
    }      

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    public void setVenta(double ven){
    	this.venta = ven;
    }
    
    public void setCompra(double compra){
    	this.compra = compra;
    }
    
    public String getNombre() {
		return nombre;
	}
}
