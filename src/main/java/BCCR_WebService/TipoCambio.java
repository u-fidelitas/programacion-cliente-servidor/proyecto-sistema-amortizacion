package BCCR_WebService;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Hans Araya
 * @version 1.0
 */
public class TipoCambio {

  private int indicador = 0; //317: Compra, 318: Venta
  private String tcFechaInicio;
  private String tcFechaFinal;
  private final String tcNombre = "FabianFdz";
  private final String tcEmail = "fdzfabian%40gmail.com";
  private final String tnSubNiveles = "N";
  private final String HOST = "https://gee.bccr.fi.cr/Indicadores/Suscripciones/WS/wsindicadoreseconomicos.asmx/ObtenerIndicadoresEconomicosXML";
  private String url;
  private final String VALUE_TAG = "NUM_VALOR";  
  private final String TOKEN = "NAAEB1D1GD";
  
  public TipoCambio(){
    setFecha();
  }
  
  public double getCompra(){
    setCompra();
    
    double valor = Double.parseDouble(getValue());
    return valor;
  }

  public double getVenta(){
    setVenta();
    
    double valor = Double.parseDouble(getValue());
    return valor;
  }

  private String getValue(){
    try {
      setUrl();
      
      //Obtiene y Parsea el XML
      String data = GetMethod.getHTML(url);
      XmlParser xml = new XmlParser(data);
      
      //Retorna el valor del tag
      return xml.getValue(VALUE_TAG);
    } catch (Exception e) {
      System.out.println("Error al obtener el valor del BCCR. Error: \n" + e.toString());
      return "0";
    }
  }
  
  private void setUrl(){
    String params = "Indicador="+indicador+"&FechaInicio="+tcFechaInicio+"&FechaFinal="+tcFechaFinal+"&Nombre="+tcNombre+"&SubNiveles="+tnSubNiveles+"&CorreoElectronico="+tcEmail+"&Token="+TOKEN;
    this.url = HOST+"?"+params;
  }
  
  private void setFecha(){
    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    this.tcFechaInicio = sdf.format(date);
    this.tcFechaFinal = tcFechaInicio;
  }
  
  private void setCompra(){
    this.indicador = 317;
  }
  
  private void setVenta(){
    this.indicador = 318;
  }
  
  @Override
  public String toString() {
      return String.format("Tipo de cambio (USD/CRC):\nCompra: ¢ %s\nVenta: ¢ %s", this.getCompra(), this.getVenta());
  }
  
}
