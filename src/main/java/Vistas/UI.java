package Vistas;

import BCCR_WebService.TipoCambio;
import Controlador.Controlador;
import DTO.SistemaAmortizacionDTO;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fabianfdz
 */
public class UI extends javax.swing.JFrame {

    TipoCambio usd;

    /**
     * Creates new form UI
     */
    public UI() {
        initComponents();

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        usd = new TipoCambio();
        compraTxt.setText("¢ " + usd.getCompra());
        ventaTxt.setText("¢ " + usd.getVenta());

        String columns[] = {"AÑO", "DEUDA", "INTERES", "AMORTIZACIÓN", "CUOTA"};
        DefaultTableModel model = new DefaultTableModel(columns, 5);

        int rowCount = model.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        resultTable.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        montoLbl = new javax.swing.JLabel();
        plazoLbl = new javax.swing.JLabel();
        interesLbl = new javax.swing.JLabel();
        metodoLbl = new javax.swing.JLabel();
        montoInp = new javax.swing.JTextField();
        monedaSimbol = new javax.swing.JLabel();
        plazoInp = new javax.swing.JSpinner();
        interesInp = new javax.swing.JSpinner();
        metodoInp = new javax.swing.JComboBox<>();
        calcularBtn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        resultTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        compraTxt = new javax.swing.JLabel();
        ventaTxt = new javax.swing.JLabel();
        metodoLbl1 = new javax.swing.JLabel();
        monedaInp = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Avenir", 1, 24)); // NOI18N
        jLabel1.setText("Sistema de Amortización");

        montoLbl.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        montoLbl.setText("Monto a calcular");

        plazoLbl.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        plazoLbl.setText("Plazo (Años)");

        interesLbl.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        interesLbl.setText("Interés Anual");

        metodoLbl.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        metodoLbl.setText("Método a utilizar");

        montoInp.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N

        monedaSimbol.setFont(new java.awt.Font("Avenir", 1, 12)); // NOI18N
        monedaSimbol.setText("¢");

        plazoInp.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        plazoInp.setModel(new javax.swing.SpinnerNumberModel(1, 1, 50, 1));

        interesInp.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        interesInp.setModel(new javax.swing.SpinnerNumberModel(17.5d, 0.0d, 100.0d, 0.1d));

        metodoInp.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        metodoInp.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Alemán", "Americano", "Francés" }));

        calcularBtn.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        calcularBtn.setText("Calcular");
        calcularBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularBtnActionPerformed(evt);
            }
        });

        resultTable.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        resultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(resultTable);

        jLabel2.setFont(new java.awt.Font("Avenir", 1, 12)); // NOI18N
        jLabel2.setText("Tipo de Cambio (USD/CRC):");

        jLabel3.setFont(new java.awt.Font("Avenir", 1, 12)); // NOI18N
        jLabel3.setText("/ Venta: ");

        jLabel4.setFont(new java.awt.Font("Avenir", 1, 12)); // NOI18N
        jLabel4.setText("Compra: ");

        compraTxt.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N

        ventaTxt.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N

        metodoLbl1.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        metodoLbl1.setText("Moneda");

        monedaInp.setFont(new java.awt.Font("Avenir", 0, 12)); // NOI18N
        monedaInp.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CRC", "USD" }));
        monedaInp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                monedaInpActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 12)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Fuente: BCCR");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(calcularBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(metodoLbl, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(montoLbl, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(metodoInp, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(monedaSimbol)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(montoInp, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(metodoLbl1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(plazoLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(plazoInp)
                            .addComponent(monedaInp, 0, 60, Short.MAX_VALUE))
                        .addGap(27, 27, 27)
                        .addComponent(interesLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(interesInp, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 453, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(compraTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ventaTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(montoLbl)
                    .addComponent(montoInp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(monedaSimbol)
                    .addComponent(interesLbl)
                    .addComponent(interesInp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(plazoLbl)
                    .addComponent(plazoInp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(metodoLbl)
                    .addComponent(metodoInp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(metodoLbl1)
                    .addComponent(monedaInp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(calcularBtn)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel4))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(compraTxt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ventaTxt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(4, 4, 4))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void calcularBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularBtnActionPerformed
        SistemaAmortizacionDTO objDTO;
        try {
            objDTO = getDatos();
            objDTO = Controlador.cualcularDatos(objDTO);
        } catch (Exception err) {
            JOptionPane.showMessageDialog(null, err.getMessage());
            return;
        }

        DefaultTableModel model = (DefaultTableModel) resultTable.getModel();

        int rowCount = model.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        Double sumInteres = 0.0, sumAmortizacion = 0.0, acumuladoDeuda = objDTO.totalPrestamo;
        String format = monedaSimbol.getText() + " %(,.2f";
        NumberFormat formatter;
        
        if (monedaInp.getSelectedIndex() == 0) {
            formatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
        } else {
            formatter = NumberFormat.getCurrencyInstance(Locale.US);
        }
        
        
        for (int i = 0; i < objDTO.getCuotasAmortizacion().size(); i++) {
            
            Double interesPeriodo = objDTO.getCuotasInteres().get(i);
            String intereses = formatter.format(interesPeriodo);
            sumInteres += objDTO.getCuotasInteres().get(i);
            Double amortizacionPeriodo = objDTO.getCuotasAmortizacion().get(i);
            String amortizacion = formatter.format(amortizacionPeriodo);
            sumAmortizacion += objDTO.getCuotasAmortizacion().get(i);
            String deuda = formatter.format(acumuladoDeuda);
            Double cuotaPeriodo = interesPeriodo + amortizacionPeriodo;
            String cuota = formatter.format(cuotaPeriodo);
            
            // Agregar a modelo
            model.addRow(new Object[]{String.valueOf(i + 1), deuda, intereses, amortizacion, cuota});
            
            // Actualiza deuda actual
            acumuladoDeuda = acumuladoDeuda - objDTO.getCuotasAmortizacion().get(i);
        }
        model.addRow(new Object[]{"", "", "Total Intereses", "Total Amortización", ""});
        model.addRow(new Object[]{"", "", String.valueOf(formatter.format(sumInteres)), String.valueOf(formatter.format(sumAmortizacion)), ""});
        resultTable.setModel(model);
    }//GEN-LAST:event_calcularBtnActionPerformed

    private void monedaInpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_monedaInpActionPerformed
        if (monedaInp.getSelectedIndex() == 0) {
            monedaSimbol.setText("¢");
        } else {
            monedaSimbol.setText("$");
        }
    }//GEN-LAST:event_monedaInpActionPerformed

    private SistemaAmortizacionDTO getDatos() throws Exception {
        String interes = interesInp.getValue().toString();
        String periodo = plazoInp.getValue().toString();
        String monto = montoInp.getText(), moneda, tipo;
        if (monedaInp.getSelectedIndex() == 0) {
            moneda = "Colon";
        } else {
            moneda = "Dolar";
        }

        switch (metodoInp.getSelectedIndex()) {
            case 0:
                tipo = "Aleman";
                break;
            case 1:
                tipo = "Americano";
                break;
            default:
                tipo = "Frances";
                break;
        }

        Double montoDbl;
        try {
            montoDbl = Double.parseDouble(monto);
        } catch (NumberFormatException ex) {
            throw new Exception("Digite un monto válido");
        }

        return new SistemaAmortizacionDTO("UI", Double.parseDouble(interes), Integer.parseInt(periodo), montoDbl, moneda, tipo);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton calcularBtn;
    private javax.swing.JLabel compraTxt;
    private javax.swing.JSpinner interesInp;
    private javax.swing.JLabel interesLbl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> metodoInp;
    private javax.swing.JLabel metodoLbl;
    private javax.swing.JLabel metodoLbl1;
    private javax.swing.JComboBox<String> monedaInp;
    private javax.swing.JLabel monedaSimbol;
    private javax.swing.JTextField montoInp;
    private javax.swing.JLabel montoLbl;
    private javax.swing.JSpinner plazoInp;
    private javax.swing.JLabel plazoLbl;
    private javax.swing.JTable resultTable;
    private javax.swing.JLabel ventaTxt;
    // End of variables declaration//GEN-END:variables
}
