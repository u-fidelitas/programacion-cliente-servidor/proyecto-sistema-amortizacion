package Modelo;

import java.rmi.RemoteException;
import java.util.*;

public class Americano extends SistemaAmortizacion {

    public Americano(double i, int nperiodos, double monto, String moneda) throws RemoteException {
        super(i,nperiodos,monto,moneda);
    }

    protected double formulaInteres() {        
        return getTotalPrestamo()*(getInteresAnual()/100);
    }

    protected double formulaAmortizacion() {
        if((getCuotasAmortizacion().size()-getPeriodo())==-1){
            return getCapitalAdeudado();
        }
        return 0.0d;
    }
}
