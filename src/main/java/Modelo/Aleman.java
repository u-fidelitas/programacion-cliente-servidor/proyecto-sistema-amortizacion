package Modelo;

import java.rmi.RemoteException;
import java.util.*;

public class Aleman extends SistemaAmortizacion {

    public Aleman(double i, int nperiodos, double monto, String moneda) throws RemoteException {
        super(i, nperiodos, monto, moneda);
    }

    protected double formulaInteres() {                
        return getCapitalAdeudado()*(getInteresAnual()/100);        
    }

    protected double formulaAmortizacion() {
        double amortizacion = getTotalPrestamo()/getPeriodo();
        rebajaCapital(amortizacion);
        return amortizacion;
    }
    
    
}
