package Modelo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class FileXML extends Bitacora {

    public FileXML() {
    }

    public void actualizarArchivo() throws Exception {
        if (!(existeBitacora("Bitacora.xml"))) {
            creaArchivo();
        }
        agregaActual();
    }

    @Override
    public void creaArchivo() throws Exception{
        Document document = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation implementation = builder.getDOMImplementation();
            document = implementation.createDocument(null, "xml", null);

            Element entrada = document.createElement("Entradas");

            document.setXmlVersion("1.0");

            document.getDocumentElement().appendChild(entrada);
            guardaConFormato(document);
        } catch (Exception e) {
            throw new Exception("Error creando el documento XML.");
        }
    }

    @Override
    public void agregaActual() throws Exception {
        // Obtenemos el document del fichero XML existente
        DocumentBuilderFactory dbf;
        try {
            dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new File("Bitacora.xml"));
            document.getDocumentElement().normalize();

            Element informacion = document.createElement("Informacion");

            Element nom = document.createElement("Nombre");
            Element monto = document.createElement("Monto");
            Element interes = document.createElement("Interes");
            Element plazo = document.createElement("Plazo");
            Element moneda = document.createElement("Moneda");
            Element sistema = document.createElement("Sistema_Amortizacion");

            Text valorNom = creaTextNode(registro.getRegistroActual().getNombre(), document);
            Text valorMonto = creaTextNode(String.valueOf(registro.getRegistroActual().getTotalPrestamo()), document);
            Text valorInt = creaTextNode(String.valueOf(registro.getRegistroActual().getInteresAnual()), document);
            Text valorPlazo = creaTextNode(String.valueOf(registro.getRegistroActual().getPeriodo()), document);
            Text valorMoneda = creaTextNode(registro.getRegistroActual().getMoneda(), document);
            Text valorSistema = creaTextNode(registro.getRegistroActual().getTipoSistema(), document);

            NodeList nodoRaiz = document.getDocumentElement().getElementsByTagName("Entradas");
            nodoRaiz.item(0).appendChild(informacion);
            informacion.appendChild(nom);
            informacion.appendChild(monto);
            informacion.appendChild(interes);
            informacion.appendChild(plazo);
            informacion.appendChild(moneda);
            informacion.appendChild(sistema);
            nom.appendChild(valorNom);
            monto.appendChild(valorMonto);
            interes.appendChild(valorInt);
            plazo.appendChild(valorPlazo);
            moneda.appendChild(valorMoneda);
            sistema.appendChild(valorSistema);

            // Guardamos la nueva estructura del fichero XML
            guardaConFormato(document);
        } catch (Exception e) {
            throw new Exception("Error creando el archivo XML.");
        }
    }

    private Text creaTextNode(String nombre, Document document) {
        return document.createTextNode(nombre);
    }

    private static void guardaConFormato(Document document) {
        try {
            TransformerFactory transFact = TransformerFactory.newInstance();

            // Formateamos el fichero. Se agrega sangrado y la cabecera de XML
            Transformer trans = transFact.newTransformer();
            //trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            trans.setOutputProperty(OutputKeys.ENCODING, "utf-8");
            trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            // Hacemos la transformacion
            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            DOMSource domSource = new DOMSource(document);
            trans.transform(domSource, sr);

            // Mostrar informacin a guardar por consola
            // Result console= new StreamResult(System.out);
            // trans.transform(domSource, console);
            try {
                // Creamos fichero para escribir en modo texto
                PrintWriter writer = new PrintWriter(new FileWriter("Bitacora.xml"));

                // Escribimos todo el rbol en el fichero
                writer.println(sw.toString());

                // Cerramos el fichero
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
