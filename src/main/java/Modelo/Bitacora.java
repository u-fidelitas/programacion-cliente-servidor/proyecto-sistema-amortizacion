package Modelo;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public abstract class Bitacora {

    public Bitacora() {
    }

    public Registro registro;

    public abstract void actualizarArchivo() throws Exception;

    public void asociaRegistro(Registro objR) {
        registro = objR;
    }

    protected boolean existeBitacora(String nombre) {
        return new File(nombre).exists();
    }

    public abstract void creaArchivo() throws Exception;

    public abstract void agregaActual() throws Exception;

}
