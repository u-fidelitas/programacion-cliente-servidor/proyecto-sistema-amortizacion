package Modelo;

import java.rmi.RemoteException;
import DTO.*;

public class Principal {

    public static SistemaAmortizacionDTO calcularCuotas(SistemaAmortizacionDTO objDTO) throws RemoteException {
        SistemaAmortizacion objSA = FactorySistemaAmorizacion.crearSistema(objDTO);
        objSA.calcularCuotas();
        objDTO.setCuotasAmortizacion(objSA.getCuotasAmortizacion());
        objDTO.setCuotasInteres(objSA.getCuotasInteres());
        objDTO.setFecha(objSA.getFecha());
        return objDTO;
    }        
}
