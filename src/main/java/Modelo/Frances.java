package Modelo;

import java.rmi.RemoteException;
import java.util.*;

public class Frances extends SistemaAmortizacion {

    public Frances(double i, int nperiodos, double monto, String moneda) throws RemoteException {
        super(i, nperiodos, monto, moneda);
    }

    protected double formulaInteres() {
        return getCapitalAdeudado() * (getInteresAnual() / 100);
    }

    protected double formulaAmortizacion() {
    	double aSubN =  (1-Math.pow(1+getInteresAnual()/100, -getPeriodo()))/(getInteresAnual()/100);
        double cuota = getTotalPrestamo()/aSubN; 
        double interes = getCapitalAdeudado() * (getInteresAnual() / 100); 
        rebajaCapital(cuota-interes);
        return cuota-interes;
    }
}
