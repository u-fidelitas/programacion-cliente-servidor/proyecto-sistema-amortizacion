package Modelo;

import java.rmi.RemoteException;
import DTO.*;

public class FactorySistemaAmorizacion {

    public static SistemaAmortizacion crearSistema(SistemaAmortizacionDTO objDTO) throws RemoteException {
        SistemaAmortizacion obj;
        double i = objDTO.getInteresAnual(), monto = objDTO.getTotalPrestamo();
        int nperiodos = objDTO.getPeriodo();
        String moneda = objDTO.getMoneda();

        if (objDTO.getTipoSistema().equalsIgnoreCase("Frances")) {
            obj = new Frances(i, nperiodos, monto, moneda);
        } else if (objDTO.getTipoSistema().equalsIgnoreCase("Aleman")) {
            obj = new Aleman(i, nperiodos, monto, moneda);
        } else {
            obj = new Americano(i, nperiodos, monto, moneda);
        }
        return obj;
    }
}