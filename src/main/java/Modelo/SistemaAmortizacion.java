package Modelo;

import java.rmi.RemoteException;
import java.util.*;

public abstract class SistemaAmortizacion {

    private final double interesAnual;
    private final int periodo;
    private final ArrayList<Double> cuotasAmortizacion;
    private final ArrayList<Double> cuotasInteres;
    private final double totalPrestamo;
    private double capitalAdeudado;
    private final String moneda;
    private final String fecha;

    public SistemaAmortizacion(double i, int nperiodos, double monto, String moneda) throws RemoteException {
        interesAnual = i;
        periodo = nperiodos;
        cuotasAmortizacion = new ArrayList<>();
        cuotasInteres = new ArrayList<>();
        totalPrestamo = monto;
        capitalAdeudado = totalPrestamo;
        this.moneda = moneda;
        fecha = Calendar.getInstance().toString();
    }

    protected abstract double formulaInteres();

    protected abstract double formulaAmortizacion();

    protected void calcularCuotas() {
        for (int i = 0; i < periodo; i++) {
            cuotasInteres.add(formulaInteres());
            cuotasAmortizacion.add(formulaAmortizacion());            
        }
    }    

    protected void rebajaCapital(double monto) {
        capitalAdeudado-=monto;
    }

    public double getInteresAnual() {
        return interesAnual;
    }

    public int getPeriodo() {
        return periodo;
    }

    public double getTotalPrestamo() {
        return totalPrestamo;
    }

    public double getCapitalAdeudado() {
        return capitalAdeudado;
    }

    public String getMoneda() {
        return moneda;
    }

    public ArrayList<Double> getCuotasAmortizacion() {
        return cuotasAmortizacion;
    }

    public ArrayList<Double> getCuotasInteres() {
        return cuotasInteres;
    }

    public String getFecha() {
        return fecha;
    }
}
