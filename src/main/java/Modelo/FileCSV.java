package Modelo;

import java.io.FileWriter;
import java.io.IOException;

public class FileCSV extends Bitacora {

    public FileCSV() {
    }

    public void actualizarArchivo() throws Exception {
        if (!(existeBitacora("Bitacora.csv"))) {
            creaArchivo();
        }
        agregaActual();
    }

    @Override
    public void creaArchivo() throws Exception {
        try {
            FileWriter writer = new FileWriter("Bitacora.csv");

            writer.append("Nombre");
            writer.append(',');
            writer.append("Monto");
            writer.append(',');
            writer.append("Interes");
            writer.append(',');
            writer.append("Plazo");
            writer.append(',');
            writer.append("Moneda");
            writer.append(',');
            writer.append("Sistema Amortizacion");
            writer.append('\n');

            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new Exception("Error creando el archivo CSV.");
        }
    }

    public void agregaActual() throws Exception {
        FileWriter pw;
        try {
            pw = new FileWriter("Bitacora.csv", true);
            pw.append(registro.getRegistroActual().getNombre());
            pw.append(",");
            pw.append(String.valueOf(registro.getRegistroActual().getTotalPrestamo()));
            pw.append(",");
            pw.append(String.valueOf(registro.getRegistroActual().getInteresAnual()));
            pw.append(",");
            pw.append(String.valueOf(registro.getRegistroActual().getPeriodo()));
            pw.append(",");
            pw.append(registro.getRegistroActual().getMoneda());
            pw.append(",");
            pw.append(registro.getRegistroActual().getTipoSistema());
            pw.append("\n");

            pw.close();
        } catch (IOException e) {
            throw new Exception("Error agregando registro al archivo CSV.");
        }
    }
}
