package Modelo;

import java.io.IOException;
import java.util.*;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import DTO.SistemaAmortizacionDTO;

public class Registro {

    public Registro() {
    }

    public SistemaAmortizacionDTO registroActual;
    public ArrayList<Bitacora> observadores = new ArrayList<Bitacora>();

    public void registrarDTO(SistemaAmortizacionDTO objDTO) throws Exception {
        registroActual = objDTO;
        notificarTodos();
    }

    public void notificarTodos() throws Exception {
        for (Bitacora bitacora : observadores) {
            bitacora.actualizarArchivo();
        }
    }

    public void suscribirse(Bitacora objB) {
        observadores.add(objB);
    }

    public SistemaAmortizacionDTO getRegistroActual() {
        return registroActual;
    }
}
