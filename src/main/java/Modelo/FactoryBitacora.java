package Modelo;

public class FactoryBitacora {

    public static Bitacora crearBitacora(String tipo) {
        switch (tipo) {
            case "XML":
                return new FileXML();
            default:
                return new FileCSV();
        }
    }

}
