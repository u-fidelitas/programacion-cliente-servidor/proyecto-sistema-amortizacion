package Controlador;

import DTO.*;
import Modelo.Bitacora;
import Modelo.FactoryBitacora;
import Modelo.FactoryRegistro;
import Modelo.Principal;
import Modelo.Registro;

public class Controlador {

    public static SistemaAmortizacionDTO cualcularDatos(SistemaAmortizacionDTO objDTO) throws Exception {
        // Creacion de archivos
        Bitacora xml = FactoryBitacora.crearBitacora("XML");
        Bitacora csv = FactoryBitacora.crearBitacora("CSV");
        
        // Registro
        Registro reg = FactoryRegistro.crearRegistro();
        
        // Agrega registro de datos a observadores
        xml.asociaRegistro(reg);
        csv.asociaRegistro(reg);
        
        // Agrega observadores a registro de datos
        reg.suscribirse(xml);
        reg.suscribirse(csv);
        
        // Agrega datos a registro
        reg.registrarDTO(objDTO);
        
        return Principal.calcularCuotas(objDTO);
    }
}
